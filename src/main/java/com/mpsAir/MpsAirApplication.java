package com.mpsAir;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class MpsAirApplication {

    public static void main(String[] args) {
        SpringApplication.run(MpsAirApplication.class, args);
    }

}
