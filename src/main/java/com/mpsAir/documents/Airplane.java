package com.mpsAir.documents;

import com.mpsAir.model.TemporaryPoint;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document("airplanes")
public class Airplane {

    @Id
    private Long id;
    private AirplaneCharacteristics airplaneCharacteristics;
    private TemporaryPoint position;
    private List<Flight> flights = new ArrayList<>();

    public Airplane(Long id, AirplaneCharacteristics airplaneCharacteristics) {
        this.id = id;
        this.airplaneCharacteristics = airplaneCharacteristics;
    }


}
