package com.mpsAir.documents;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@AllArgsConstructor
@ToString
@Document("airplane_characteristics")
public class AirplaneCharacteristics {

    @Id
    private Integer id;
    private Double maxSpeed;
    private Double maxAcceleration;
    private Double altitudeChangeRate;
    private Double courseChangeRate;
}
