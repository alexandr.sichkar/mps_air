package com.mpsAir.documents;

import com.mpsAir.model.TemporaryPoint;
import com.mpsAir.model.WayPoint;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document("flights")
public class Flight {
    @Id
    private Long number;
    private List<WayPoint> wayPoints = new ArrayList<>();
    private List<TemporaryPoint> passedPoints = new ArrayList<>();
    private Integer flightDuration;

    public Flight(Long number, List<WayPoint> wayPoints) {
        this.number = number;
        this.wayPoints = wayPoints;
        flightDuration = 0;
    }
}
