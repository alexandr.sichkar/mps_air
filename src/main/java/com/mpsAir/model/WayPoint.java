package com.mpsAir.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class WayPoint {
    private Double latitude;
    private Double longitude;
    private Double flightAltitude;
    private Double flightSpeed;
}
