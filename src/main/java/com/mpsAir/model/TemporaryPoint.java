package com.mpsAir.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TemporaryPoint {
    private Double latitude;
    private Double longitude;
    private Double flightAltitude;
    private Double flightSpeed;
    private Double flightCourse;
}
