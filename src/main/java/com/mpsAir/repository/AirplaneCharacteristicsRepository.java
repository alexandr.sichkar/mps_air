package com.mpsAir.repository;

import com.mpsAir.documents.AirplaneCharacteristics;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AirplaneCharacteristicsRepository extends MongoRepository<AirplaneCharacteristics, Long> {
}
