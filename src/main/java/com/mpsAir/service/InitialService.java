package com.mpsAir.service;

import com.mpsAir.documents.Airplane;
import com.mpsAir.documents.AirplaneCharacteristics;
import com.mpsAir.documents.Flight;
import com.mpsAir.model.WayPoint;
import com.mpsAir.repository.AirplaneCharacteristicsRepository;
import com.mpsAir.repository.AirplaneRepository;
import com.mpsAir.repository.FlightRepository;
import jakarta.annotation.PostConstruct;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Setter(onMethod = @__({@Autowired}))
@FieldDefaults(level = AccessLevel.PRIVATE)
@Service
public class InitialService {
    AirplaneRepository airplaneRepository;
    AirplaneCharacteristicsRepository airplaneCharacteristicsRepository;
    FlightRepository flightRepository;
    FlightService flightService;

    //создам 3 самолета, 3 характеристики, 3 полета
    private static List<WayPoint> wayPoints1 = new ArrayList<>();

    static {
        wayPoints1.add(new WayPoint(48.541238, 35.105089, 0.0, 0.0));
        wayPoints1.add(new WayPoint(48.586594, 35.053866, 2500.0, 200.0));
        wayPoints1.add(new WayPoint(48.664659, 35.021147, 2500.0, 200.0));
    }

    private static List<WayPoint> wayPoints2 = new ArrayList<>();

    static {
        wayPoints2.add(new WayPoint(48.541238, 35.105089, 0.0, 0.0));
        wayPoints2.add(new WayPoint(48.586594, 35.053866, 2000.0, 180.0));
        wayPoints2.add(new WayPoint(48.664659, 35.021147, 2000.0, 180.0));
    }

    private static List<WayPoint> wayPoints3 = new ArrayList<>();

    static {
        wayPoints3.add(new WayPoint(48.541238, 35.105089, 0.0, 0.0));
        wayPoints3.add(new WayPoint(48.586594, 35.053866, 1800.0, 300.0));
        wayPoints3.add(new WayPoint(48.664659, 35.021147, 1800.0, 300.0));
    }

    private static Flight flight1 = new Flight(1L, wayPoints1);
    private static Flight flight2 = new Flight(2L, wayPoints2);
    private static Flight flight3 = new Flight(3L, wayPoints3);

    private static AirplaneCharacteristics airplaneCharacteristicsItem1 = new AirplaneCharacteristics(
            1, 250.0, 50.0, 300.0, 10.0
    );
    private static AirplaneCharacteristics airplaneCharacteristicsItem2 = new AirplaneCharacteristics(
            2, 200.0, 60.0, 250.0, 10.0
    );
    private static AirplaneCharacteristics airplaneCharacteristicsItem3 = new AirplaneCharacteristics(
            3, 350.0, 40.0, 220.0, 10.0
    );

    private static Airplane airplane1 = new Airplane(1L, airplaneCharacteristicsItem1);
    private static Airplane airplane2 = new Airplane(2L, airplaneCharacteristicsItem2);
    private static Airplane airplane3 = new Airplane(3L, airplaneCharacteristicsItem3);

    private static List<Flight> flights = List.of(flight1, flight2, flight3);
    private static List<AirplaneCharacteristics> airplaneCharacteristics = List.of(airplaneCharacteristicsItem1,
            airplaneCharacteristicsItem2, airplaneCharacteristicsItem3);
    private static List<Airplane> airplanes = List.of(airplane1, airplane2, airplane3);


    @PostConstruct
    public void init() {
        //сохраню в объекты в базу
        flightRepository.saveAll(flights);
        airplaneCharacteristicsRepository.saveAll(airplaneCharacteristics);
        airplaneRepository.saveAll(airplanes);

        flightService.goFlight();
    }
}