package com.mpsAir.service;

import com.mpsAir.documents.Airplane;
import com.mpsAir.documents.Flight;
import com.mpsAir.model.TemporaryPoint;
import com.mpsAir.model.WayPoint;
import com.mpsAir.repository.AirplaneRepository;
import com.mpsAir.repository.FlightRepository;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Setter(onMethod = @__({@Autowired}))
@FieldDefaults(level = AccessLevel.PRIVATE)
@Service
public class FlightService {
    PlaneCalculation planeCalculation;
    AirplaneRepository airplaneRepository;
    FlightRepository flightRepository;

    private static final Log LOG = LogFactory.getLog(FlightService.class);

    public void goFlight() {
        //начну моделирование полета
        flight(1L, 1L);
        flight(1L, 2L);
        flight(1L, 3L);
        flight(2L, 2L);
        flight(2L, 3L);
        flight(3L, 3L);
        flight(3L, 1L);
    }

    public void flight(Long airplaneId, Long flightId) {
        Airplane airplane = airplaneRepository.findById(airplaneId)
                .orElseThrow(() -> new NoSuchElementException(
                        "Can't find Airplane by ID " + airplaneId));
        Flight flight = flightRepository.findById(flightId)
                .orElseThrow(() -> new NoSuchElementException(
                        "Can't find Flight by ID " + flightId));

        LOG.info("Flight " + flight.getNumber() + " started by airplane " + airplane.getId());
        LOG.info("Airplane " + airplane.getId() + " has " + airplane.getFlights().size() + " flight with total time in air "
                + airplane.getFlights().stream().mapToInt(Flight::getFlightDuration).sum());
        LinkedList<WayPoint> wayPoints = new LinkedList<>(flight.getWayPoints());
        Double startCourse = Math.round((Math.random() * (360 - 0) + 0) * 10d) / 10d; //стартовый курс ЛА, от 0.0 до 360.0 (градусы)
        airplane.setPosition(new TemporaryPoint(
                wayPoints.get(0).getLatitude(), wayPoints.get(0).getLongitude(),
                wayPoints.get(0).getFlightAltitude(), wayPoints.get(0).getFlightSpeed(), null));
        if (airplane.getFlights().size() > 0) {
            airplane.getFlights().add(flight);
        } else {
            airplane.setFlights(new ArrayList<>(List.of(flight)));
        }
        airplaneRepository.save(airplane);
        while (wayPoints.size() >= 2) {
            if (airplane.getPosition().getFlightCourse() == null) {
                airplane.getPosition().setFlightCourse(startCourse);
                startCourse = airplane.getPosition().getFlightCourse();
            }

            TemporaryPoint temporaryPoint = planeCalculation.calculateRoute(
                    airplane.getAirplaneCharacteristics(), wayPoints, startCourse);

            startCourse = temporaryPoint.getFlightCourse();

            WayPoint wayPointEnd = wayPoints.get(1);

            //запишем данные, переопределим стартовую точку на текущую
            WayPoint currentPoint = new WayPoint(temporaryPoint.getLatitude(),
                    temporaryPoint.getLongitude(),
                    temporaryPoint.getFlightAltitude(),
                    temporaryPoint.getFlightSpeed());

            //проверим не прошли ли wayPointEnd
            if (wayPointEnd.getLatitude() > flight.getWayPoints().get(0).getLatitude()
                    && wayPointEnd.getLongitude() > flight.getWayPoints().get(0).getLongitude()
                    && wayPointEnd.getLatitude() <= currentPoint.getLatitude()
                    && wayPointEnd.getLongitude() <= currentPoint.getLongitude()
                    || wayPointEnd.getLatitude() > flight.getWayPoints().get(0).getLatitude()
                    && wayPointEnd.getLongitude() < flight.getWayPoints().get(0).getLongitude()
                    && wayPointEnd.getLatitude() <= currentPoint.getLatitude()
                    && wayPointEnd.getLongitude() >= currentPoint.getLongitude()) {
                wayPoints.set(1, currentPoint);
                wayPoints.remove(0);
            } else if (wayPointEnd.getLatitude() < flight.getWayPoints().get(0).getLatitude()
                    && wayPointEnd.getLongitude() < flight.getWayPoints().get(0).getLongitude()
                    && wayPointEnd.getLatitude() >= currentPoint.getLatitude()
                    && wayPointEnd.getLongitude() >= currentPoint.getLongitude()
                    || wayPointEnd.getLatitude() < flight.getWayPoints().get(0).getLatitude()
                    && wayPointEnd.getLongitude() > flight.getWayPoints().get(0).getLongitude()
                    && wayPointEnd.getLatitude() >= currentPoint.getLatitude()
                    && wayPointEnd.getLongitude() <= currentPoint.getLongitude()
            ) {
                wayPoints.set(1, currentPoint);
                wayPoints.remove(0);
            } else {
                wayPoints.set(0, currentPoint);
            }
            flight.setFlightDuration(flight.getFlightDuration() + 1);
            flight.getPassedPoints().add(temporaryPoint);
            airplane.getFlights().set(airplane.getFlights().size()-1, flight);
            airplane.setPosition(temporaryPoint);
            airplaneRepository.save(airplane);
        }
        LOG.info("   Flight " + flight.getNumber() + " completed with total time " + flight.getFlightDuration() +  ", by airplane " + airplane.getId());
        LOG.info("");
    }
}