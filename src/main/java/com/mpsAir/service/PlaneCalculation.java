package com.mpsAir.service;

import com.mpsAir.documents.AirplaneCharacteristics;
import com.mpsAir.model.TemporaryPoint;
import com.mpsAir.model.WayPoint;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Setter(onMethod = @__({@Autowired}))
@FieldDefaults(level = AccessLevel.PRIVATE)
@Service
public class PlaneCalculation {

    //метод для моделирования полета
    public TemporaryPoint calculateRoute(AirplaneCharacteristics characteristics, List<WayPoint> wayPoints, double startCourse) {
        double snapTime = 1.0; //интервал времени для контроля полета (сек)
        double optimalCourse; //оптимальный курс ЛА между двумя точками (градусы)
        double currentCourse; //текущий курс (градусы)

        double currentSpeed; //текущая скорость (м/с)
        double currentAltitude; //текущая высота (м)
        double currentPath; //пройденный путь между промежуточными точками (м)
        double pathProection; //проекция пути на землю (м)

        double latitudeWeight = 111000.0; //принимаю расстояние на 1 градус широты  111000м
        double longitudeWeight = 70000.0; //принимаю расстояние на 1 градус долготы  70000м
        double currentLatitude; //текущая/стартовая широта (градусы)
        double currentLongitude; //текущая/стартовая долгота (градусы)

        double absPath; //проекция расстояния между текущей точкой (wayPointStart) и wayPointEnd
        WayPoint wayPointStart = wayPoints.get(0);
        WayPoint wayPointEnd = wayPoints.get(1);

        double currentAccelerate = characteristics.getMaxAcceleration(); //текущее ускорение (м/с2)

        //определяем скорость в TemporaryPoint
        currentSpeed = Math.round((wayPointStart.getFlightSpeed() + currentAccelerate * snapTime)*10000.0)/10000.0;
        if (currentSpeed > wayPointEnd.getFlightSpeed()) {
            currentSpeed = wayPointEnd.getFlightSpeed();
        }
        //определяем пройденный путь в TemporaryPoint
        currentPath = Math.round((currentSpeed * snapTime)*10000.0)/10000.0;
        //определяем высоту в TemporaryPoint
        currentAltitude = characteristics.getAltitudeChangeRate() + wayPointStart.getFlightAltitude();
        if (currentAltitude > wayPointEnd.getFlightAltitude()) {
            currentAltitude = wayPointEnd.getFlightAltitude();
        }
        //определяем проекцию общего пути, который необходимо пройти между точками
        absPath = Math.sqrt(Math.pow(((wayPointEnd.getLongitude() - wayPointStart.getLongitude())*longitudeWeight), 2)
                + Math.pow(((wayPointEnd.getLatitude() - wayPointStart.getLatitude()))*latitudeWeight, 2));
        //определяем курс по которому придем в TemporaryPoint
        optimalCourse = Math.round((Math.acos((wayPointStart.getLongitude() - wayPointEnd.getLongitude()) * longitudeWeight/ absPath))*10.0)/10.0;
        double courseChange = characteristics.getCourseChangeRate();
            if (Math.abs(startCourse - optimalCourse) > 180 && startCourse > optimalCourse) {
                currentCourse = startCourse + courseChange;
            } else if(startCourse > optimalCourse && startCourse - courseChange > optimalCourse) {
                currentCourse = startCourse - courseChange;
            } else {
                currentCourse = optimalCourse;
            }
            if (currentCourse >= 360) {
                currentCourse = currentCourse - 360;
            }
            if (currentCourse < 0) {
                currentCourse = currentCourse + 360;
            }
        //определяем проекцию пройденного пути на географические координаты
        pathProection = Math.round((Math.sqrt(Math.pow(currentPath, 2) + Math.pow(currentAltitude, 2)))*10000.0)/10000.0;
        //определяем координаты в TemporaryPoint
        double pathLatitude = Math.abs(pathProection * Math.cos(currentCourse));
        double pathLongitude = Math.abs(pathProection * Math.sin(currentCourse));
        if (wayPointEnd.getLatitude() - wayPointStart.getLatitude() > 0) {
            currentLatitude = Math.round((wayPointStart.getLatitude() + pathLatitude / latitudeWeight)*1000000.0)/1000000.0;
        } else {
            currentLatitude = Math.round((wayPointStart.getLatitude() - pathLatitude / latitudeWeight)*1000000.0)/1000000.0;
        }
        if (wayPointEnd.getLongitude() - wayPointStart.getLongitude() > 0) {
            currentLongitude = Math.round((wayPointStart.getLongitude() + pathLongitude / longitudeWeight)*1000000.0)/1000000.0;
        } else {
            currentLongitude = Math.round((wayPointStart.getLongitude() - pathLongitude / longitudeWeight)*1000000.0)/1000000.0;
        }
        //вернем TemporaryPoint
        return new TemporaryPoint(currentLatitude,
                currentLongitude, currentAltitude, currentSpeed, currentCourse);
    }
}
